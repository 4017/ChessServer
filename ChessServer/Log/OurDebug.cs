﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessServer.Log {

    public static class OurDebug {
        private static string lasterrormsg = "";
        public static bool blog = true;
        public static bool berror = true;
        public static bool bwarn = true;

        public static void Log ( object message ) {
            if ( blog )
                WriteInfo ( message.ToString () );
        }

        public static void Log ( object message, Object context ) {
            // 暂时只做了string的日志。
            if ( blog )
                WriteInfo ( message.ToString (), context );
        }

        internal static void SetLastErrorMsg ( string message ) {
            lasterrormsg = message;
        }

        internal static string GetLastErrorMsg () {
            return lasterrormsg;
        }

        public static void LogFormat ( string format, params object [] args ) {
            if ( blog )
                WriteInfo ( format, args );
        }

        //public static void LogFormat(Object context, string format, params object[] args)
        //{
        //    WriteInfo(format, args);
        //}

        public static void LogWarning ( object message ) {
            WriteWarn ( message.ToString () );
        }

        public static void LogWarning ( object message, Object context ) {
            WriteWarn ( message.ToString () );
        }

        public static void LogWarningFormat ( string format, params object [] args ) {
            WriteWarn ( format, args );
        }

        public static void LogWarningFormat ( Object context, string format, params object [] args ) {
            WriteWarn ( format, args );
        }

        public static void LogError ( object message ) {
            WriteError ( message.ToString () );
        }

        public static void LogError ( object message, Object context ) {
            WriteError ( message.ToString () );
        }

        public static void LogErrorFormat ( string format, params object [] args ) {
            WriteError ( format, args );
        }

        //public static void LogErrorFormat ( Object context, string format, params object [] args ) {
        //    WriteError ( format, args );
        //}

        public static void LogException ( Exception exception ) {
            WriteError ( exception.Message );
        }

        public static void LogException ( Exception exception, Object context ) {
            WriteError ( exception.Message );
        }
        /// <summary>
        /// 堆栈跟踪输出
        /// </summary>
        /// <returns></returns>
        public static string GetStackTrace () {
            StackTrace st = new StackTrace ( true );
            string stackIndent = "  ";
            StringBuilder builder = new StringBuilder ();
            int count = st.FrameCount;
            for ( int i = 1; i < count; i++ ) {
                StackFrame sf = st.GetFrame ( i );
                //得到错误的方法
                var method = sf.GetMethod ();
                string className = method.DeclaringType != null ? method.DeclaringType.FullName : "Unknow type";
                string fileName = sf.GetFileName ();
                builder.AppendFormat ( "{0} at {1}.{2}", stackIndent, className, method );
                if ( !string.IsNullOrEmpty ( fileName ) ) {
                    builder.AppendFormat ( " file {0}:line {1}", fileName, sf.GetFileLineNumber () );
                }
                builder.AppendLine ();
            }

            return builder.ToString ();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void Write ( string message, params object [] args ) {
            Write ( message, false, args );
        }

        /// <summary>
        /// 只在编译器的DEBUG下输出到TraceLog目录
        /// </summary>
        /// <param name="message"></param>
        /// <param name="includStackTrace"></param>
        /// <param name="args"></param>
        private static void Write ( string message, bool includStackTrace, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            if ( includStackTrace ) {
                LogHelper.WriteDebug ( str + "\r\n" + GetStackTrace () );
            }
            else {
                LogHelper.WriteDebug ( str );
            }
        }

        /// <summary>
        /// 只在编译器的DEBUG下输出到Info目录
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteInfo ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteInfo ( str );
        }

        /// <summary>
        /// 记录出错日志到Error目录下
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteError ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteError ( str );
        }

        /// <summary>
        /// 记录出错日志到Warn目录下
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteWarn ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteWarn ( str );
        }
        /// <summary>
        /// 记录出错日志到Complement目录下
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteComplement ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteComplement ( str );
        }

        /// <summary>
        /// Write to custom log
        /// </summary>
        /// <param name="name">dir name</param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteTo ( string name, string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( name + ">>" + message, args );
            }
            LogHelper.WriteTo ( name, str );
        }

        private const string LoggerSqlName = "Sql";
        /// <summary>
        /// Write sql error.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteSqlError ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( LoggerSqlName + ">>" + message, args );
            }
            LogHelper.WriteTo ( LoggerSqlName, str );
        }

        /// <summary>
        /// 在DEBUG和Release下输出到Info目录
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void ReleaseWrite ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteInfo ( str );
        }
        /// <summary>
        /// 记录出错日志到TraceLog目录下
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void ReleaseWriteDebug ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteDebug ( str );
        }
        /// <summary>
        /// 记录出错日志到Fatal目录下
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void ReleaseWriteFatal ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( "Trace>>" + message, args );
            }
            LogHelper.WriteFatal ( str );
        }

        /// <summary>
        /// Write line
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void WriteLine ( string message, params object [] args ) {
            string str = message;
            if ( args.Length > 0 ) {
                str = string.Format ( message, args );
            }
            LogHelper.WriteLine ( str );
        }
    }
}
