﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessServer.Net;

namespace ChessServer {
    class Start : IStart {
        private volatile bool m_isrunning = false;

        private static Start _instance;

        public static Start instance {
            get {
                if ( _instance == null ) {
                    _instance = new Start ();
                }
                return _instance;
            }
        }

        private Start () {
        }

        public int init () {
            Console.WriteLine ( "current dir :" + System.IO.Directory.GetCurrentDirectory () );
            var ret = NetHelper.instance.Init ();
            if ( ret != 0 ) {
                return ret;
            }

            ret = MongoHelper.Init ();
            if ( ret != 0 ) {
                return ret;
            }

            return 0;
        }

        public int run () {
            m_isrunning = true;
            
            var ret = NetHelper.instance.Run ();
            if ( ret != 0 ) {
                return ret;
            }

            return ServerCmd.Run ();
        }

        public int stop () {
            MongoHelper.Stop ();
            var ret = TcpSocketServer.Stop ();
            if ( ret != 0 ) {
                return ret;
            }
            return 0;
        }

        public bool isrunning () {
            return m_isrunning;
        }
    }
}
