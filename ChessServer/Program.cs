﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessServer.Log;

namespace ChessServer {
    class Program {
        static void Main ( string [] args ) {
            var curpath = System.IO.Directory.GetCurrentDirectory ();
            OurDebug.LogFormat ( "server starting...\ncurrent directory:{0}", curpath );
            var starter = Start.instance;
            try {
                var ret = starter.init ();
                if ( ret != 0 ) {
                    OurDebug.LogErrorFormat ( "server init fail.. errormsg:{0} errorcode:{1} ", OurDebug.GetLastErrorMsg (), ret );
                    return;
                }
                // 开跑
                ret = starter.run ();
                if ( ret != 0 ) {
                    OurDebug.LogErrorFormat ( "server run fail.. errormsg:{0} errorcode:{1} ", OurDebug.GetLastErrorMsg (), ret );
                    return;
                }
            }
            catch (Exception ex) {
                OurDebug.LogException ( ex );
                if ( starter.isrunning () ) {
                    var ret = starter.stop ();
                    if ( ret != 0 ) {
                        OurDebug.LogErrorFormat ( "server stop fail.. errormsg:{0} errorcode:{1} ", OurDebug.GetLastErrorMsg (), ret );
                        return;
                    }
                }
            }
            finally {
                OurDebug.Log ( "Server Closed." );
                Console.ReadKey ();
            }
        }
    }
}
